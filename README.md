# NG Spice  : Electronique From Scratch #4 

**Résumé de l'introduction a NGspice 	faite par Rancune lors de l'épisode 4 d'Electronique From Scratch (le 17/06/2022)**

* la vidéo :   https://www.youtube.com/watch?v=BE13NAfmzYg    (ngspice à partir de 55min)
* la chaine twitch pour les lives :  https://www.twitch.tv/rancune_
* le blog de Rancune : https://rancune.org

## les principaux types de simulations de ngspice
3 types de simulation principales

- OP (Operating Point) : courant continu, pas de variation une fois que le circuit a pris ses valeurs.

- AC :  courant changeant :  point de fonctionnement et on oscille autour du point de fonctionnement. sources de tension sinusoidales, etc... 

- Transient : phénomènes transitoires (non répétitif)  Par exemple : La charge du condensateur ne se passe qu'une fois

## Exemple simple de définition d'un circuit ( nets et composants )
-  V1 source de tension (V) numéro 1  reliée entre le nœud A et le nœud 0 : valeur = tension continue de 10V
-  R1 Résistance (R) numéro 1 reliée entre le nœud A et le noeud B ; valeur 50Ω
-  R2 Résistance (R) numéro 2 reliée entre le nœud B et le noeud 0 ; valeur 30Ω

Le nœud 0 est obligatoire dans spîce et indique la reference de tension (potentiel 0)

```

.title Diviseur de tension
V1 A 0 dc 10
R1 A B 50
R2 B 0 30
.end

```

## Documentation ngspice 
- [site officiel NGSpice](http://ngspice.sourceforge.net)
- [documentation NGspice](http://ngspice.sourceforge.net/docs/ngspice-html-manual/manual.xhtml)
  - Les éléments de circuits disponibles : [2.1.3](http://ngspice.sourceforge.net/docs/ngspice-html-manual/manual.xhtml#magicparlabel-283)
 

### Résistance (R)
doc :   [3.3.1](http://ngspice.sourceforge.net/docs/ngspice-html-manual/manual.xhtml#subsec_Resistors)
```
RXXXXXXX n+ n- <resistance|r=>value <ac=val> <m=val> <scale=val> 
+ <temp=val> <dtemp=val> <tc1=val> <tc2=val>
+ <noisy=0|1>
```

### Source de tension (V)
doc  : [4.1](http://ngspice.sourceforge.net/docs/ngspice-html-manual/manual.xhtml#sec_Independent_Sources_for) 

```
VXXXXXXX N+ N- <<DC> DC/TRAN VALUE> <AC <ACMAG <ACPHASE>>>
+ <DISTOF1 <F1MAG <F1PHASE>>> <DISTOF2 <F2MAG <F2PHASE>>>
IYYYYYYY N+ N- <<DC> DC/TRAN VALUE> <AC <ACMAG <ACPHASE>>>
+ <DISTOF1 <F1MAG <F1PHASE>>> <DISTOF2 <F2MAG <F2PHASE>>>
```

## Exécution d'une simulation ngspice
### chargement du fichier
- Executer directement le fichier contenant la definition du circuit :
```
ngspice <nom_du_fichier>

```

- **OU** charger le fichier dans la CLI ngspice avec `source`
 
```
ngspice                         
[...]
ngspice 1 -> source <nom_du_fichier.cir>
```


### Lancement analyse en point de fonctionnement (OP)
#### Manuellement dans la CLI ngspice
```
op

Doing analysis at TEMP = 27.000000 and TNOM = 27.000000
```

La simu est effectuée, on peut imprimer les résultats. Par exemple la tension au noeud B ave c`print B`  ou encore `print all` pour imprimer toutes les valeurs disponibles.
`display`  pour connaitre la nature des éléments mesurés (on voit qu'on peut avoir accès au courant de l'alimentation)

```
print B 
b=  3.750000e+00
print all
a = 1.000000e+01
b = 3.750000e+00
v1#branch = -1.25000e-01
ngspice 3 -> display
Here are the vectors currently active:

Title: Diviseur de tension
Name: op1 (Operating Point)
Date: Wed Jun 22 19:24:38  2022

    a                   : voltage, real, 1 long [default scale]
    b                   : voltage, real, 1 long
    v1#branch           : current, real, 1 long

```




#### Automatiquement en ajoutant des "contrôles" dans le fichier .cir

Il est possible d'ajouter les "controles" (les mesures à effectuer) dans le fichier de simu. ils seront exécuter au chartgement du fichier automatiquement. (plus pratique)
```

.title Diviseur de tension
V1 A 0 dc 10
R1 A B 50
R2 B 0 30

.control
op
print B
.endc

.end

```


**Sortie : **

```
Circuit: Diviseur de tension

Doing analysis at TEMP = 27.000000 and TNOM = 27.000000


No. of Data Rows : 1
b = 3.750000e+00
```
#### Test de 'charge' du pont diviseur
On peut verifier en ajoutant (sous R2 ...) une résistance de charge "élevée : `Rcharge B 0 10k`  que la tension du pont diviseur ne change pas beaucoup

Par contre avec une charge "faible" `Rcharge B 0 10` la tension s'ecroule.


### Simulation 'transient'
- la source de tension envoie un pulse de 0 à 5V au bout de 10ms. temps de montée et de descente : 1µs , durée de la pulsation 1s. Il est possible ensuite d'indiquer une période avant de recommencer)
- lancement d'une analyse 'transient' entre 50µs et 50ms  (avant la fin du pulse donc)
```

.title Diviseur de tension
V1 A 0 dc 0 PULSE (0 5 10m 1u 1u 1 )
R1 A B 50
R2 B 0 30

.control
  tran 50u 50m
  plot A B
.endc

.end

```

**Sortie : (graph)**
![Tension d'un pont diviseur selon variation de la charge](./img/graph_transient_pont_diviseur_simple.png)


###  Variation de valeur d'un composant
- La valeur de la resistance de Charge du pont diviseur varie de 100Ω à 5Ω par pas de -10Ω
- Un calcul OP est fait à chaque changement et est enregistré dans un fichier `out.txt`
```

.title Diviseur de tension
V1 A 0 dc 10
R1 A B 50
R2 B 0 30
Rcharge B 0 500

.control
set filetype=ascii
let start_r = 100
let stop_r = 5
let delta_r = 10
let r = start_r
while r ge stop_r
  alter Rcharge r
  op
  write out.txt B
  set appendwrite
  let r = r - delta_r
end
.endc

.end


```


**Sortie : (extrait du fichier out.txt)**
```
Title: Diviseur de tension
Date: Wed Jun 22 19:38:53  2022
Plotname: Operating Point
Flags: real
No. Variables: 2
No. Points: 1
Variables:
    0   a   voltage
    1   b   voltage
Values:
 0  1.000000000000000e+01
    3.157894736842105e+00

Title: Diviseur de tension
Date: Wed Jun 22 19:38:53  2022
Plotname: Operating Point
Flags: real
No. Variables: 2
No. Points: 1
Variables:
    0   a   voltage
    1   b   voltage
Values:
 0  1.000000000000000e+01
    3.103448275862069e+00


[...]


Title: Diviseur de tension
Date: Wed Jun 22 19:38:53  2022
Plotname: Operating Point
Flags: real
No. Variables: 2
No. Points: 1
Variables:
    0   a   voltage
    1   b   voltage
Values:
 0  1.000000000000000e+01
    1.935483870967742e+00

Title: Diviseur de tension
Date: Wed Jun 22 19:38:53  2022
Plotname: Operating Point
Flags: real
No. Variables: 2
No. Points: 1
Variables:
    0   a   voltage
    1   b   voltage
Values:
 0  1.000000000000000e+01
    1.304347826086957e+00
	
```
