# Simulation NGSpice de circuit basiques


## les simulations
### Fichier `.cir` 

 - Les fichiers `.cir` sont directement utilisable par ngspice.

```
ngspice mon_fichier.cir
```


### Utilisation de kicad pour les circuit et la simulation

 - Les circuits sont définis dans Kicad, (fichiers `.kicad_sch`) et on utilise ensuite le simulteur NGspice de Kicad (menu inspecter > Simulateur)
 - Les donnés spices sont soit dans le schéma, soit dans le fichier `.wbk` a charger dans l'ecran du simulteur ngspice kicad.






## Résutats
### pont  diviseur chargé
La valeur de la résistance de charge (Rc) varie. La courbe rouge mesure la tension du pont diviseur, la courbe bleue est la tension d'alimentation
- Quand Rc est faible et proche des résistances du pont diviseur, la tension du pont s'effondre . 
- Quand Rc est elevée par rapport aux résistances du pont, la tension du pont est proche de sa tension nominale (à vide)

![Tension de sortie d'un pont diviseur selon la variation de la charge](../img/pont_diviseur_simu_kicad.png)



### pont de Wheatstone
- La tension du pont est à 0V quand il est équilibré (toutes les résistances à 50Ω) cf. le curseur en pointillé
- Quand une des résistances varie (R4 ici), le pont est "déséquilibré" et sa tension change.
 
![Variation de la tension d'un pont de Wheatstone déséquilibré (variation de R4)](../img/wheatstone_simu_kicad.png)



### courant-tension dans une diode
- Pour toute tension d'alimentation négative et inférieure à environ 0.7V, la diode est bloquée : aucun courant ne passe.
- Dès qu'on atteint la tension de seuil de la diode (environ 0.7V), la diode devient passante et le courant la traversant grimpe très vite. Sans limitation de courant par un résistance correctement dimensionnée (ici la résistance de 10mΩ est très faible ) on voit que le courant traversant la diode est rapidement très important (une diode simple n'y survivrait pas longtemps)

![diagramme courant-tension d'une diode](../img/diode_simu_kicad.png)





### Charge-décharge d'un condensateur
- Un pulse est de 10V et d'une durée de 0.75s est appliqué au circuit (courbe bleue) , ce qui charge le condensateur via R1 (courbe rouge). 
- A la fin du pulse, la tension d'alimentation passe à 0V, le condensateur se décharge.

#### constante de temps
La constante de temps du circuit se calcule en multipliant la vaeur de R (en Ω) par la valeur de C (en F)
```
R x C = 100.10³ x 10¯⁶ = 0.1s
```

- On retrouve graphiquement cette valeur en regardant le temps de charge à 63% (ici 6.3V pour 10V)
- On considère qu'a 5 fois la constante de temps, le condensateur est chargé (charge > 99%)


![chargee et décharge d'un condensateur](../img/charge_condo_simu_kicad.png)

